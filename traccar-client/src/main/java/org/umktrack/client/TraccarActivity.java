/*
 * Copyright 2012 - 2014 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.umktrack.client;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Main user interface
 */
@SuppressWarnings("deprecation")
public class TraccarActivity extends Activity {

    static final String CUSTOM_INTENT= "org.umktrack.intent.GET_LAST_POSITION";
    public static final String LOG_TAG = "TraccarActivity";

    public static final String KEY_ID = "id";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PORT = "port";
    public static final String KEY_INTERVAL = "interval";
    public static final String KEY_PROVIDER = "provider";
    public static final String KEY_STATUS = "status";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        addPreferencesFromResource(R.xml.preferences);
        initPreferences();
        boolean isServiceWasRunning = true;
        if (!isServiceRunning( (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE) )) {
            startService(new Intent(this, TraccarService.class));
            isServiceWasRunning = false;
        }
        Intent intent = getIntent();

        if (intent != null ) {
            Log.d(LOG_TAG, "requested last coordinates "+intent.getAction());
            Intent responseIntent = new Intent();
            Bundle bundle = new Bundle();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            bundle.putFloat("lat", sharedPreferences.getFloat("for1C_lat", 45));
            bundle.putFloat("lng", sharedPreferences.getFloat("for1C_lng", 39));
            bundle.putLong("time", sharedPreferences.getLong("for1C_time", -333));

            String guid;
            if (sharedPreferences.contains(KEY_ID)) {
                guid = sharedPreferences.getString(KEY_ID, "");
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                guid = "android-" + telephonyManager.getDeviceId();
            }
            bundle.putString("guid", guid);
//            isServiceWasRunning ? "NOT_STARTED" : "",
            responseIntent.putExtras(bundle);
            setResult(Activity.RESULT_OK, responseIntent);

            Log.d(LOG_TAG, "Result setted");
        }
        finish();
//        int pid = android.os.Process.myPid();
//        android.os.Process.killProcess(pid);
//        System.exit(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isServiceRunning( (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE) ))
            startService(new Intent(this, TraccarService.class));
        finish();
    }

    public static boolean isServiceRunning( ActivityManager manager ) {
        return getRunningService(manager) != null;
    }
    public static RunningServiceInfo getRunningService( ActivityManager manager ) {
        //ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (TraccarService.class.getName().equals(service.service.getClassName())) {
                return service;
            }
        }
        return null;
    }

    private void initPreferences() {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

//        if (!sharedPreferences.contains(KEY_ID)) {
            String id = "android-" + telephonyManager.getDeviceId();
            sharedPreferences.edit().putString(KEY_ID, id).commit();
//        }
    }

}

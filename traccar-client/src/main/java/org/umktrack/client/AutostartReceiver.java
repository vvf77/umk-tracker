/*
 * Copyright 2013 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.umktrack.client;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class AutostartReceiver extends BroadcastReceiver {

    final int restartAlarmInterval = 3*60*1000;
    public static final String LOG_TAG = "AutostartReceiver";
    static final String CUSTOM_INTENT= "org.umktrack.intent.GET_LAST_POSITION";
    static final String KEY_ID="id";
 
    @Override
    public void onReceive(Context context, Intent intent) {
        // ACTION_TIME_TICK
        // May be ACTION_POWER_CONNECTED  ACTION_POWER_DISCONNECTED
        Log.d(LOG_TAG, "AutostartReceiver started");
        Log.d(LOG_TAG, "Action = " + intent.getAction());

        ActivityManager.RunningServiceInfo serviceInfo = TraccarActivity.getRunningService( (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE) );

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            context.startService(new Intent(context, TraccarService.class));
            Log.d(LOG_TAG, "ACTION_BOOT_COMPLETED started TraccarService");
        } else {
            if (serviceInfo != null) {
                //TraccarService service = (TraccarService)serviceInfo.;
                context.startService(new Intent(context, TraccarService.class));
            }
        }

        if (intent.getAction().equals(CUSTOM_INTENT)){
            Log.d(LOG_TAG, "requested last coordinates");
            Bundle bundle = getResultExtras(true);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            bundle.putFloat("lat", sharedPreferences.getFloat("for1C_lat", 45));
            bundle.putFloat("lng", sharedPreferences.getFloat("for1C_lng", 39));
            bundle.putLong("time", sharedPreferences.getLong("for1C_time", -333));

            String guid;
            if (sharedPreferences.contains(KEY_ID)){
                guid=sharedPreferences.getString(KEY_ID, "");
            }else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                guid = "android-" + telephonyManager.getDeviceId();
            }
            bundle.putString("guid", guid);

            //setResult(Activity.RESULT_OK, wasnt_started ? "NOT_STARTED" : "", bundle);
            Log.d(LOG_TAG, "Result setted");
        }

        Intent i = new Intent(context.getApplicationContext(), TraccarService.class);
        PendingIntent pi = PendingIntent.getService(context.getApplicationContext(), 0, i, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pi); // cancel any existing alarms
        am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + restartAlarmInterval,
                restartAlarmInterval, pi);

    }



}

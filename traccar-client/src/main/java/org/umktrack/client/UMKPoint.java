package org.umktrack.client;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by vvf on 30.01.15.
 */
public class UMKPoint extends Location{
    private short numberOfValidPoints;
    private long lastTime;

    public UMKPoint(Cursor cursor) {
        // Create point filling from data of current position of cursor
        super(new Location("db"));
        for(int i=0, l=cursor.getColumnCount(); i<l;i++){
            String column_name = cursor.getColumnName(i);
            if( column_name.equalsIgnoreCase("timestamp") ){
                setTime( cursor.getLong(i));
            }else
            if( column_name.equalsIgnoreCase("Accuracy") ) {
                setAccuracy(cursor.getFloat(i));
            }else
            if( column_name.equalsIgnoreCase("Course") ) {
                setBearing(cursor.getFloat(i));
            }else
            if( column_name.equalsIgnoreCase("Longitude") ) {
                setLongitude(cursor.getDouble(i));
            }else
            if( column_name.equalsIgnoreCase("Latitude") ) {
                setLatitude(cursor.getDouble(i));
            }else
            if( column_name.equalsIgnoreCase("Accelerate") ) {
                setAccelerate(cursor.getDouble(i));
            }else
            if( column_name.equalsIgnoreCase("lastTime") ) {
                lastTime = cursor.getLong(i);
            }else
            if( column_name.equalsIgnoreCase("numberOfValidPoints") ) {
                numberOfValidPoints = (short) cursor.getInt(i);
            }
        }
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    private double accelerate;

    public UMKPoint(Location l){
        super(l);
        numberOfValidPoints=1;
        lastTime = l.getTime();
    }
    public void incNumOfPoints(){
        numberOfValidPoints ++;
    }

    public void setLastTime(long t){
        lastTime=t;
    }
    public long getLastTime(){ return lastTime;}

//    public void save(){
//        // TODO: update to DB
//    }
    public String getOldUrlString(){
        return "&lat=" + this.getLatitude() + "&lon=" + this.getLongitude() +
                (this.hasAccuracy() ? "" : "&accuracy=" + this.getAccuracy()) +
                (this.hasSpeed() ? "" : "&speed=" + (this.getSpeed())) +
                "&npoints=" + this.numberOfValidPoints +
                "&timestamp=" + (getTime()/1000);
        // "&timestamp=" + (this.Timestamp.Ticks/10000);// D/M/Y:H:M:S; here timestamp.Ticks - is not the same timestamp as in all usix-based systems (time from 00:00:00 01.01.1970 GMT)
        //"&time=" + this.Timestamp.ToString(@"dd'/'MM'/'yyy':'HH':'mm':'ss");// D/M/Y:H:M:S;
    }

    public byte[] serializeCompact(int actualServerApiVersion)
    {
        int cur =0;
        ByteBuffer resultArr = ByteBuffer.allocate( UMKPoint.compactSize(actualServerApiVersion) );
        resultArr.order(ByteOrder.LITTLE_ENDIAN);
        resultArr.putLong( getTime()/1000 );
        resultArr.putDouble( getLatitude() );
        resultArr.putDouble( getLongitude() );
        resultArr.putDouble( getAccuracy() );
        resultArr.putDouble( getBearing() );
        /*
        double speed = this.speed;
        if (!double.IsNaN(Location.Speed) && speed < 0)
            speed = Location.Speed;
        */
        resultArr.putDouble( getSpeed() );
        resultArr.putDouble(accelerate);
        resultArr.putShort(numberOfValidPoints);

        if (actualServerApiVersion >= 1)
        {
            resultArr.putLong( lastTime/1000 );
        }
        return resultArr.array();
    }
    public static short compactSize(int actualServerApiVersion)
    {
        // result will depends on actualServerApiVersion
        if (actualServerApiVersion == 1)
        {
            return 66;
        }
        return 58;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put("Timestamp", getTime());
        cv.put("Accuracy", getAccuracy());
        cv.put("Course", getBearing() );
        cv.put("Longitude", getLongitude() );
        cv.put("Latitude", getLatitude() );
        cv.put("lastTime", getLastTime() );
        cv.put("accelerate", accelerate );
        cv.put("numberOfValidPoints", numberOfValidPoints );
        return cv;
    }
}

package org.umktrack.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/**
 * Created by vvf on 30.01.15.
 * here also will be networking included
 */
interface NetworkEvents{
    void OnNetworkCheckResult(boolean isConnection, Object detail);
    void OnSentPacket(JSONObject detail);
    void OnAbort();
}
public class Saver implements NetworkEvents {

    private static final String LOG_TAG = "Saver";
    private final String DeviceGUID;
    UMKPoint lastPoint = null;
    UMKPoint lastPoint2 = null;
    float lastDistance;
    float lastSpeed;
    int fileLinesCounter = 0;
    public boolean isSendingProcess = false;
    long startSendTime = 0;

    private Networking netw;

    Context applicationContext = TraccarService.getContextOfApplication();
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);

    long POINTS_MIN_ACCURACY        = Long.valueOf(sharedPreferences.getString("POINTS_MIN_ACCURACY", "50"));
    long POINTS_MAX_VALID_SPEED     = Long.valueOf(sharedPreferences.getString("POINTS_MAX_VALID_SPEED", "150"));
    long POINTS_MAX_ACCELERATION    = Long.valueOf(sharedPreferences.getString("POINTS_MAX_ACCELERATION", "150"));
    long POINTS_MAX_STAING_DISTANCE = Long.valueOf(sharedPreferences.getString("POINTS_MAX_STAING_DISTANCE", "50"));
    long POINTS_MAX_STAYING_SPEED   = Long.valueOf(sharedPreferences.getString("POINTS_MAX_STAYING_SPEED", "3"));
    long NET_MINUTES_BETWEEN_SESSIONS   = Long.valueOf(sharedPreferences.getString("NET_MINUTES_BETWEEN_SESSIONS", "5"));
    long MAX_QUEUE_POINTS           = Long.valueOf(sharedPreferences.getString("MAX_QUEUE_POINTS", "100"));
    long SENDING_TIMEOUT            = Long.valueOf(sharedPreferences.getString("NET_SENDING_TIMEOUT", "25000"));

    ArrayList<UMKPoint> pointsQueue = new ArrayList<UMKPoint>((int)MAX_QUEUE_POINTS);
    private long lastSessionTime = 0;
    PointsDBHelper sqLiteOpenHelper = null;

    public Saver(String srv, int port, String guid) {
        DeviceGUID = guid;
        netw = new Networking(this, srv, port, guid);

        SharedPreferences.OnSharedPreferenceChangeListener sharedPreferencesChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,    String cfg){
                Log.d(LOG_TAG,"Preference update " + cfg);
                try {
                    if (cfg.equals("POINTS_MIN_ACCURACY")) {
                        POINTS_MIN_ACCURACY = Long.valueOf(sharedPreferences.getString("POINTS_MIN_ACCURACY", "50"));
                    } else if (cfg.equals("POINTS_MAX_VALID_SPEED")) {
                        POINTS_MAX_VALID_SPEED = Long.valueOf(sharedPreferences.getString("POINTS_MAX_VALID_SPEED", "150"));
                    } else if (cfg.equals("POINTS_MAX_ACCELERATION")) {
                        POINTS_MAX_ACCELERATION = Long.valueOf(sharedPreferences.getString("POINTS_MAX_ACCELERATION", "150"));
                    } else if (cfg.equals("POINTS_MAX_STAING_DISTANCE")) {
                        POINTS_MAX_STAING_DISTANCE = Long.valueOf(sharedPreferences.getString("POINTS_MAX_STAING_DISTANCE", "50"));
                    } else if (cfg.equals("POINTS_MAX_STAYING_SPEED")) {
                        POINTS_MAX_STAYING_SPEED = Long.valueOf(sharedPreferences.getString("POINTS_MAX_STAYING_SPEED", "3"));
                    } else if (cfg.equals("NET_MINUTES_BETWEEN_SESSIONS")) {
                        NET_MINUTES_BETWEEN_SESSIONS = Long.valueOf(sharedPreferences.getString("NET_MINUTES_BETWEEN_SESSIONS", "15"));
                    } else if (cfg.equals("MAX_QUEUE_POINTS")) {
                        MAX_QUEUE_POINTS = Long.valueOf(sharedPreferences.getString("MAX_QUEUE_POINTS", "10"));
                    } else if (cfg.equals("SENDING_TIMEOUT")) {
                        SENDING_TIMEOUT = Long.valueOf(sharedPreferences.getString("SENDING_TIMEOUT", "25000"));
                    }
                } catch (Exception error) {
                    Log.w(LOG_TAG, error);
                }


            }
        };

        sharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferencesChangeListener);
    }
    public void setContext(Context c){
        if( netw!= null )netw.setContext(c);
        sqLiteOpenHelper = new PointsDBHelper(c);
        fileLinesCounter = sqLiteOpenHelper.getCountOfUnsentPoints();
        if( fileLinesCounter > 0){
            setLastPoint(sqLiteOpenHelper.getLastPoint());
            Log.d(LOG_TAG, "There was already several unsent points in database " + fileLinesCounter);
        }
    }
    private void setLastPoint(UMKPoint p){
        if (lastPoint == null) {lastPoint2 = p;} else {lastPoint2 = lastPoint;}
        lastPoint = p;
        SharedPreferences.Editor config = sharedPreferences.edit();
        config.putFloat("for1C_lat", ((float) p.getLatitude()));
        config.putFloat("for1C_lng", ((float) p.getLongitude()));
        config.putLong("for1C_time", p.getLastTime());
        config.commit();
        //Toast.makeText( netw.context, "UMK: last point time="+p.getLastTime(), Toast.LENGTH_LONG).show();
    }

    public void setAppVersion(String v){
        if( netw!= null )netw.setAppVersion(v);
    }

    public void setNewServer(String srv,int port){
        netw = new Networking(this, srv,port, DeviceGUID);
    }
    public void Stop(){
        // is never used yet
        // TODO: close database, and make other
        if( sqLiteOpenHelper != null)
            sqLiteOpenHelper.close();
    }
    private boolean allowAddPoint(UMKPoint new_point)
    {

        Log.d(LOG_TAG, "New point come. " + "getAccuracy:"+ new_point.getAccuracy() + " min: " + POINTS_MIN_ACCURACY);
        // if ( new_point.hasAccuracy() && new_point.getAccuracy() > POINTS_MIN_ACCURACY )
        if ( new_point.hasAccuracy() && new_point.getAccuracy() > 50 )
        {
            Log.d(LOG_TAG,"Point rejected by bad accuracy");
            return false;
        }

        if ( !new_point.hasAccuracy())
        {
            return false;
        }

        if (lastPoint == null)
        {
            setLastPoint(new_point);
            Log.d(LOG_TAG,"Allow adding point because it first point which I know");
            return true;
        }
        float distance = lastPoint.distanceTo(new_point);


        long TimeDiff = (new_point.getTime() - lastPoint.getTime()) / 1000;
        if ( TimeDiff < 1)
        {
            //if (isDebugLogging)
            Log.d(LOG_TAG, "Timestampt looks like in previous point. diff=" + TimeDiff);
            return false;
        }
        float speed = Float.NaN;
        if( new_point.hasSpeed() ){
            speed = new_point.getSpeed();
        }else {
            try {
                //speed = (distance / 1000) / (TimeDiff / 3600);
                speed = (distance / TimeDiff) * (3600/1000);
                new_point.setSpeed( speed );
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        // filters by:
        // -- by speed:
        if (speed > POINTS_MAX_VALID_SPEED) {
            Log.d(LOG_TAG, "Point rejected by bad speed");
            Toast.makeText( netw.context, "UMK Speed = "+speed+" ("+POINTS_MAX_VALID_SPEED+") Distance = "+distance+" TimeDiff = " + TimeDiff, Toast.LENGTH_LONG).show();
            Log.d(LOG_TAG, "UMK Speed = "+speed+" ("+POINTS_MAX_VALID_SPEED+") Distance = "+distance+" TimeDiff = " + TimeDiff);

            return false;
        }

        Log.d(LOG_TAG, "UMK Speed = "+speed+" ("+POINTS_MAX_VALID_SPEED+") Distance = "+distance+" TimeDiff = " + TimeDiff);

        float acceleration = Float.NaN;
        if (lastPoint != null)
        {
            acceleration = (float)((lastSpeed - speed) / 3.6) / TimeDiff; // km per hour convert to meters per second
            Log.d(LOG_TAG, "UMK acceleration = "+(acceleration));

            if (acceleration > 120)
            {
                //if (isDebugLogging)
                Log.d(LOG_TAG, "Invalid by acceleration from" + lastSpeed + " to " + speed + " is " + acceleration + " m/s");
                return false;
            }
        }
        // here need fast calculation of heading
        new_point.setAccelerate( acceleration );
        lastDistance = distance;
        lastSpeed = speed;
        float distance2 = lastPoint2.distanceTo(new_point);
        setLastPoint(new_point);
        //if (isDebugLogging) Log("Setted new last point. acceleration = " + acceleration);

        // Filters done. Here valid point.
        // Check - may ignore this point:
        /*if (distance < POINTS_MAX_STAING_DISTANCE // not far out of previous
                // speed and headind dont't changed - we have not to save it.
                && (speed <= POINTS_MAX_STAYING_SPEED
                || new_point.getAccuracy() < distance // radius of points spot more than distance from prev point (prev point within this points spot)
        ))
        {
            Log.d(LOG_TAG, "Its not far of previous point - just increment old point");
            incrementLastPoint(new_point);
            return false;
        }*/



        Log.d(LOG_TAG, "distance2 = " + distance2 + " distance =  " + distance);

        if (distance2 < distance)
        {
            if (pointsQueue.size()>1) {pointsQueue.remove(pointsQueue.size()-1);}
            Log.d(LOG_TAG, "Removed previous point. distance2 < distance ");
        }

        /*if (pointsQueue.size() > 3) {
            UMKPoint point3 = pointsQueue.get((pointsQueue.size() - 4));
            UMKPoint point1 = pointsQueue.get((pointsQueue.size() - 1));

            if (point1.distanceTo(point3)< distance) {
                pointsQueue.remove(pointsQueue.size()-1);
                pointsQueue.remove(pointsQueue.size()-1);

            }

        }*/



        return true;
    }

    private void incrementLastPoint(UMKPoint p) // uses when point doesn't change but this fact must be fixed. (Instead of no information about location)
    {
        if (pointsQueue.size() <= 0)
        {
            addPoint(p);
            return;
        }
        UMKPoint last_in_queue=pointsQueue.get(pointsQueue.size()-1);
        //if (last_in_queue.Location.GetDistanceTo(p.Location) < 3)
        //{
        if( sqLiteOpenHelper!=null) sqLiteOpenHelper.increasePointCounter(last_in_queue.getTime(), p.getTime());

        last_in_queue.incNumOfPoints();
        last_in_queue.setLastTime( p.getTime() );
        //last_in_queue.save();
    }

    private void addPoint(UMKPoint p) {
        pointsQueue.add(p);
        while( pointsQueue.size() > MAX_QUEUE_POINTS ) {
            pointsQueue.remove(0);
        }
        // TODO: add point to database
        // TODO: if previous point has numberPoints > 1 then update it in database too
//        sqLiteOpenHelper.addPoint( p );
        FileWriteLine(p);
        Log.d(LOG_TAG, "DB: points in database: " + sqLiteOpenHelper.getCountOfUnsentPoints() + " fileLinesCounter="+fileLinesCounter + " p.time=" + p.getTime() + " pointsQueue.size=" +pointsQueue.size());
    }

    public void saveNewPoint(Location position){
        UMKPoint new_point = new UMKPoint(position);
        if (allowAddPoint(new_point))
        {
            addPoint(new_point);
        }
    }

    private boolean FileWriteLine(UMKPoint line) {
        if(sqLiteOpenHelper != null && sqLiteOpenHelper.addPoint(line)) { // try to add point to database
            fileLinesCounter ++;
            return true;
        }
        return false;
    }
    public void tick(){
        // Log.d(LOG_TAG, "Tick at time=" + new Date().getTime()+" last="+lastSessionTime);
        // Log.d(LOG_TAG, "lastSessionTime=" + lastSessionTime + ", diff=" + (new Date().getTime() - lastSessionTime));
        if( isSendingProcess && new Date().getTime() - startSendTime > SENDING_TIMEOUT*1000){
            Log.w(LOG_TAG, "do cancelSendData by sending timeout");
            cancelSendData();
            return;
        }
        if( (new Date().getTime() - lastSessionTime )<=NET_MINUTES_BETWEEN_SESSIONS * 60000 ){
            // Ignore -- next session after no less than MINUTES_BETWEEN_SESSIONS minutes
//            Log.d(LOG_TAG, "Network session starts in " + String.valueOf((NET_MINUTES_BETWEEN_SESSIONS * 60000) - (new Date().getTime() - lastSessionTime)));
            return;
        }

        // TODO: check available networks
        // TODO: try to connect to server
        if (!isSendingProcess)
        {
            Log.d(LOG_TAG, "do check network");
            netw.checkNetwork();
        }else{
            Log.w(LOG_TAG, "sending in progress");
        }
    }


    private void tryToTurnMobileBroadbandOn() {

    }

    private void cancelSendData() {
        netw.abort();
    }

    private void startSendData(){
        isSendingProcess = true;
        startSendTime = new Date().getTime();
        Log.d("startSendData","Begin transmit points data " + startSendTime);
        /*
        TODO: send from prev files
        if (isNeedToCheckPreviousFiles && previousFilesSenderThread == null)
        {
            previousFilesSenderThread = new System.Threading.Thread(new System.Threading.ThreadStart(checkPreviousFiles));
            previousFilesSenderThread.Start();
        }
        */
        // 1. check if we have some to send?
        // lookup any other own files except current sorting by name (files name include timestamp, so they will be sorted by time)
        // If no other files: check queue
        if (fileLinesCounter > pointsQueue.size())
        {
            // send file lines first.
            // switch new points to another file, and start read and send this file
            //String lastFileName = main_queue_file;
            //initFilename(); // next point wil be saved to next file
                //harvester.
            Log.d(LOG_TAG, "Send point from file. lines=" + fileLinesCounter + " Queue.count=" + pointsQueue.size());
            sendFromFile();
        }
        else
        {
            Log.d(LOG_TAG, "Send point queue=" + pointsQueue.size());
            sendFromQueue();
        }
        // in sending progress - set sendingTicks=0 and after send to
        lastSessionTime = new Date().getTime();
    }

    private void sendFromQueue() {
        //if (harvester.isDebugLogging) harvester.Log("Send point from queue " + pointsQueue.Count);
        while (pointsQueue.size() > 0)
        {
            UMKPoint pnt;
            pnt = pointsQueue.get(0);
            // skip points older than are on server
            if (pnt.getTime() >= netw.serverLastPointTimestamp * 1000)
            {
                break;
            }
            pointsQueue.remove(0);

            Log.d(LOG_TAG, "Remove point because it older than existing on server. " + pnt.getTime() + " >=  " + (netw.serverLastPointTimestamp*1000) + " : " + pointsQueue.size());
        }
        if (pointsQueue.size() >0 )
        {
            netw.sendPointsPacket(pointsQueue);

        }else{
            // TODO: no new points (by time)
            isSendingProcess = false;
        }
    }

    private void sendFromFile() {
        // TODO: implement sending from database
        // temporary: because no actual sending
        isSendingProcess = true;
        ArrayList<UMKPoint> n = sqLiteOpenHelper.getUnsentPoints(0);
        //n.addAll( pointsQueue );
        if( n.size() >0 ) {
            Log.d("sendFromFile", "Readed from DB points count=" + n.size());
            Log.d("sendFromFile", "In queue count =" + pointsQueue.size());
            Log.d("sendFromFile", "Send read data: " + n);

            netw.sendPointsPacket(n);
        }
    }

    @Override
    public void OnNetworkCheckResult(boolean isConnection, Object detail){
        Log.d(LOG_TAG, "Network checking Done, has connection: " + isConnection);
        if (isConnection)
        {
            // send all collected data
            startSendData();
        }
        else
        {
            tryToTurnMobileBroadbandOn();
            isSendingProcess = false;
            //  if failed - wait 10-th part of time out between sessions
            lastSessionTime = new Date().getTime() - NET_MINUTES_BETWEEN_SESSIONS * 60000;

        }
    }

    @Override
    public void OnSentPacket(JSONObject detail) {
        // TODO: clear database and clear queue
        Log.d(LOG_TAG, "Sending Packet Done");
        if( detail != null ) {
            lastSessionTime = new Date().getTime();
            try {
                lastSessionTime = detail.getLong("lastTimestamp") * 1000;
                //Log.i(LOG_TAG, "pointsRecieved=" +detail.getInt("pointsRecieved"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if( sqLiteOpenHelper != null && lastPoint != null ) {
                Log.d(LOG_TAG, "Clear db");
                // netw.serverLastPointTimestamp
                sqLiteOpenHelper.dropOlderThan(lastPoint.getTime()); // TODO: here lastPoint might be already another new point
            }
            pointsQueue.clear();// TODO: not clear yet!!! Due to sending process might come another points
            // TODO: destroy main points queue and set second queue as a main and create new second queue
        }else{
            // TODO: clear second queue
        }
        isSendingProcess = false;
    }

    @Override
    public void OnAbort() {
        isSendingProcess = false;
    }



    static class Networking {

        private final int implementedServerApi = 1;

        private String server;
        private String imei;
        private HttpURLConnection urlConnection;
        public long serverLastPointTimestamp;
        public NetworkEvents events;

        private int actualServerApiVersion = 0;

        private AsyncTask<ByteBuffer, Void, Boolean> sessionTask = null;
        private Context context;

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        private String appVersion;

        public void setContext(Context context) {
            this.context = context;
            Toast.makeText( context, "UMK: Begin networking", Toast.LENGTH_LONG).show();
        }

        public Networking(NetworkEvents ev, String srv, int port, String guid) {
            imei = guid;
            events = ev;
            server = "http://" + srv + ":" + String.valueOf(port);
        }

        public void abort() {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (sessionTask != null) {
                sessionTask.cancel(true);
            }
            events.OnAbort();
        }

        private abstract class SessionTask extends AsyncTask<ByteBuffer, Void, Boolean> {

            @Override
            protected Boolean doInBackground(ByteBuffer... params) {
                ByteBuffer buffer = null;
                if (params.length > 0) {
                    buffer = params[0];
                }
                try {
                    URL url = getUrl();
                    if (url == null) return false;
                    urlConnection = (HttpURLConnection) url.openConnection();
                    Log.d(LOG_TAG,"##### Opened connection: to " + url.toString());
                    urlConnection.setUseCaches(false);
                    if (buffer != null) {
                        urlConnection.setRequestMethod("POST");
                        Log.d(LOG_TAG,"##### Data size: " + buffer.position()+"  "+buffer.toString());
                        //urlConnection.setFixedLengthStreamingMode(buffer.position());
                        urlConnection.setDoOutput(true);
                        // TODO: set header indicates this is gzipped content
                        GZIPOutputStream zos = new GZIPOutputStream(new BufferedOutputStream(urlConnection.getOutputStream()));
                        zos.write(buffer.array());
                        zos.close();
                    }
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Log.d(LOG_TAG,"response code:" + urlConnection.getResponseCode()+" enc="+urlConnection.getContentEncoding());
                    // TODO: do json reading from stream
                    byte buf[] = new byte[200]; // TODO: Make overflow-safe reading
                    int res = in.read(buf);
                    String responseText = new String(buf, "UTF-8").substring(0, res);
                    in.close();
                    Log.d(LOG_TAG, "read from buffer:" + responseText);
                    return parseResponse(responseText);

                } catch (IOException e) {
                    Log.d(LOG_TAG, "IOException: " + e.getMessage());
                    Log.e(LOG_TAG, "IOException: " + e.toString());
                    urlConnection = null;
                    notifyOfFailure();
                    return false;
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    urlConnection = null;
                }
            }

            protected abstract URL getUrl();

            protected abstract void notifyOfFailure();

            protected abstract Boolean parseResponse(String responseText);
        }
        private class SessionTaskChecker extends SessionTask {
            @Override
            protected URL getUrl(){
                URL url;
                String surl;
                surl = server + "/tablets/api/checker?guid=" + imei  + "&v=an-"+appVersion;// + version; // TODO: add app version to params
                try {
                    url = new URL(surl);
                }catch(MalformedURLException e) {
                    Log.d(LOG_TAG, "MalformedURLException (invalid url): " + e.getMessage());
                    Log.d(LOG_TAG, server + surl);
                    urlConnection = null;
                    notifyOfFailure();
                    return null;
                }
                return url;
            }
            @Override
            protected void notifyOfFailure() {
                events.OnNetworkCheckResult(false, null);
            }
            @Override
            protected Boolean parseResponse(String responseText) {
                parseServerCommands(responseText);
                events.OnNetworkCheckResult(true, responseText);
                return true;
            }

            private void parseServerCommands( String message ){
                //HttpURLConnection resp
                Iterator entries = urlConnection.getHeaderFields().entrySet().iterator();
                String msgType = null;
                while(entries.hasNext())
                {
                    Map.Entry<String, List<String>> header = (Map.Entry<String, List<String>>) entries.next();
                    String header_name = header.getKey();

                    //Log.d("Headers", "["+ header_name +"]=" + header.getValue().get(0));
                    if( header_name == null) {
                    }
                    else if (header_name.equals("X-MSG-Type")) {
                        msgType = header.getValue().get(0);
                    }
                    else if (header_name.equals("X-Last-Timestamp"))
                    {
                        // Convert from UNIX_TIMESTAMP to this Ticks/ 10000
                        serverLastPointTimestamp = Long.valueOf(header.getValue().get(0)) *1000;
                        Log.d(LOG_TAG, "serverLastPointTimestamp = " + serverLastPointTimestamp);
                    }
                    else if (header_name.equals("X-API-Version"))
                    {
                        int currentServerApiVersion = Integer.valueOf(header.getValue().get(0));
                        // use lowerst of implemented on server and client version of API
                        actualServerApiVersion = implementedServerApi < currentServerApiVersion ? implementedServerApi : currentServerApiVersion;
                        Log.d(LOG_TAG, "Server API v " + currentServerApiVersion + " will be used API v "+actualServerApiVersion);
                    }
                    else if (header_name.startsWith("X-SetConfig-") ){
                        String cfg = header_name.substring(12);
                        String val = header.getValue().get(0);
                        if( cfg.matches("^[a-zA-Z0-9_-]+$") /*&& Regex.IsMatch(cfg = k.Substring(12), @"^[a-zA-Z0-9_-]+$") TODO:make compare by */ ) {

                            //header.getValue().get(0)
                            Log.d(LOG_TAG, "set config var: " + cfg + " = " + val);

                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context) ;
                            sharedPreferences.edit().putString(cfg, val).commit();
                        }else{
                            Log.e(LOG_TAG, "Invalid config var:"+cfg);
                        }
                    }
                }

                if( msgType != null && msgType.equalsIgnoreCase("txt") ){
                    // TODO: notify some who will show message to user.
                    Toast.makeText( context, message, Toast.LENGTH_LONG).show();
                }
            }

        }

        private class SessionTaskTrack extends SessionTaskChecker {
            @Override
            protected URL getUrl() {
                URL url = null;
                String surl;
                surl = server + "/tablets/api/track?imei=" + imei + "&v=an-"+appVersion;
                surl += "&api=" + actualServerApiVersion;
                try {
                    url = new URL(surl);
                }catch(MalformedURLException e) {
                    Log.d(LOG_TAG, "MalformedURLException (invalid url): " + e.getMessage());
                    Log.d(LOG_TAG, server + surl);
                    urlConnection = null;
                    events.OnSentPacket(null);
                    return null;
                }
                return url;
            }

            @Override
            protected Boolean parseResponse(String responseText) {
                JSONObject j = null;
                try {
                    j = new JSONObject(responseText);
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Error json parse from " + responseText + "\n error=" + e.getMessage());
                }
                events.OnSentPacket(j);
                return true;
            }

            @Override
            protected void notifyOfFailure() {
                events.OnSentPacket(null);
            }
        }

        public void sendPointsPacket(final List<UMKPoint> pointSet) {

            short OnePointSize = UMKPoint.compactSize(actualServerApiVersion);

            ByteBuffer buffer = ByteBuffer.allocate( 2 + OnePointSize*pointSet.size() );
            // server expects little-endian byte order
            buffer.order( ByteOrder.LITTLE_ENDIAN );
            buffer.putShort((short) pointSet.size());
            for( UMKPoint point : pointSet){
                buffer.put( point.serializeCompact(actualServerApiVersion) );
            }
            sessionTask = new SessionTaskTrack();
            sessionTask.execute(buffer);
        }

        public void checkNetwork() {
            sessionTask = new SessionTaskChecker();
            sessionTask.execute();
        }
    }
}

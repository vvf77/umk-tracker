package org.umktrack.client;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by vvf on 09.02.15.
 */
public class PointsDBHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = "PointsDBHelper";
    private static final int LOCAL_DB_VERSION = 1;
    private static final String POINTS_TABLE_NAME = "points";
    public PointsDBHelper(Context context) {
        super(context, "UMKPoints", null, LOCAL_DB_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
                sqLiteDatabase.execSQL("CREATE TABLE "+POINTS_TABLE_NAME+" ( "+
                                "Timestamp long, " +
                                "Accuracy float, " +
                                "Course float, " +
                                "Longitude double, " +
                                "Latitude double, " +
                                "lastTime long, "+
                                "accelerate double, "+
                                "numberOfValidPoints int )"
                );
        sqLiteDatabase.execSQL("CREATE TABLE log ( "+
                "Timestamp long, logLine TEXT )"
        );


//                return null;
//            }
//        }.execute();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        // There isn`t any other versions
    }

    public boolean addPoint(UMKPoint point) {
        //getWritableDatabase().execSQL("INSERT INTO "+POINTS_TABLE_NAME+" VALUES (?, ?, ?, ?, ?, ?, ?, ?)", new Object[]{
        //   1,2,3,4,5,6,7,8
        //});
        try {

            return getWritableDatabase().insert(POINTS_TABLE_NAME, null, point.getContentValues()) == 1;

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void dropOlderThan(long timestamp){
        getWritableDatabase().delete(POINTS_TABLE_NAME, "lastTime < ?", new String[]{ String.valueOf(timestamp) });
    }
    
    public void increasePointCounter(long pointTimestamp, long newLastTimestamp){
        ContentValues cv = new ContentValues(2);
        if( pointTimestamp ==0 ){
            Cursor cursor = getReadableDatabase().query(POINTS_TABLE_NAME, new String[]{"Timestamp"},null,null,null,null,"Timestamp DESC", "1");
            if( cursor.getCount() <=0 ){
                return;
            }
            pointTimestamp = cursor.getLong(0);
        }
        if( pointTimestamp ==0 ) {
            return;
        }
        cv.put("lastTime", newLastTimestamp);
        cv.put("numberOfValidPoints","numberOfValidPoints+1");
        getWritableDatabase().update(POINTS_TABLE_NAME, cv, "Timestamp=?", new String[]{ String.valueOf(pointTimestamp)});
        Log.d(LOG_TAG, "Increased number of points in last row");
    }


    public ArrayList<UMKPoint> getUnsentPoints(int size) {
        Cursor cursor = getReadableDatabase().query(POINTS_TABLE_NAME, null, null, null, null, null, "Timestamp", "300" );
        cursor.moveToFirst();
        ArrayList<UMKPoint> result = new ArrayList<UMKPoint>(cursor.getCount() + size);
        do{
            result.add(new UMKPoint(cursor));
        }while (cursor.moveToNext());
        return result;
    }

    public int getCountOfUnsentPoints() {
        Cursor cursor = getReadableDatabase().rawQuery("select count(*) from " + POINTS_TABLE_NAME, null);// query(POINTS_TABLE_NAME, null, null, null, null, null, "Timestamp" );
        cursor.moveToFirst();
        if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
            return cursor.getInt(0);
        } else {
            return 0;
        }
    }

    public UMKPoint getLastPoint() {
        // TODO: read and return last point from database
        Cursor cursor = getReadableDatabase().query(POINTS_TABLE_NAME, null, null, null,null,null, "Timestamp DESC","1");
        Log.d(LOG_TAG, "Points in db: "+ cursor.getCount());
        if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
            cursor.moveToFirst();
            return new UMKPoint(cursor);
        }
        return null;
    }
}

/*
 * Copyright 2012 - 2014 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.umktrack.client;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
//import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

/**
 * Background service
 */
public class TraccarService extends Service {

    public static final String LOG_TAG = "TrackService";

    private String id;
    private String address;
    private int port;
    private int interval;
    private String provider;

    private SharedPreferences sharedPreferences;
    private Saver saver;
    private PositionProvider positionProvider;
    
    private PowerManager.WakeLock wakeLock;
    private String version;

    public static Context contextOfApplication;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        turnGPSOn();
        //TODO do something useful

        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {

        contextOfApplication = getApplicationContext();

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
//        wakeLock.acquire();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            id = sharedPreferences.getString(TraccarActivity.KEY_ID, null);
            address = sharedPreferences.getString(TraccarActivity.KEY_ADDRESS, null);
            provider = sharedPreferences.getString(TraccarActivity.KEY_PROVIDER, null);
            port = Integer.valueOf(sharedPreferences.getString(TraccarActivity.KEY_PORT, null));
            interval = Integer.valueOf(sharedPreferences.getString(TraccarActivity.KEY_INTERVAL, null));
        } catch (Exception error) {
            Log.w(LOG_TAG, error);
        }

        saver  = new Saver(address, port, id);
        saver.setContext( this );
        saver.setAppVersion( getVersion() );

        positionProvider = new PositionProvider(this, provider, interval * 1000, positionListener, clock);
        positionProvider.startUpdates();

        sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);




    }

    public static Context getContextOfApplication(){
        return contextOfApplication;
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind: "+intent.getAction());
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG,"Service destroyed");

        if (sharedPreferences != null) {
        	sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
        }

        if (positionProvider != null) {
        	positionProvider.stopUpdates();
        }

        if( saver != null ) {
            saver.Stop();
        }
//        wakeLock.release();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startService(new Intent(TraccarService.this, TraccarService.class));
//            }
//        },2000);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public double getBatteryLevel() {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR) {
            Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            if( batteryIntent == null ){
                return 0;
            }
            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);
            return (level * 100.0) / scale;
        } else {
            return 0;
        }
    }
    private PositionProvider.TickListener clock = new PositionProvider.TickListener() {
        @Override
        public void onTick() {
            saver.tick();
        }
    };
    private PositionProvider.PositionListener positionListener = new PositionProvider.PositionListener() {

        @Override
        public void onPositionUpdate(Location location) {

            if (location != null) {
                Log.d(LOG_TAG,"Location update");
                if( location != null ){
                    saver.saveNewPoint(location);
                }else{
                    Log.w(LOG_TAG, "No location");
                }
            }
        }
    };
    public String getVersion(){
        try {
            return getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
        }catch (Exception e){
            return "0.0.0.0";
        }
    }
    OnSharedPreferenceChangeListener preferenceChangeListener = new OnSharedPreferenceChangeListener() {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Log.d(LOG_TAG,"Preference update " + key);
            try {
                if (key.equals(TraccarActivity.KEY_ADDRESS)) {

                    address = sharedPreferences.getString(TraccarActivity.KEY_ADDRESS, null);
                    //clientController.setNewServer(address, port);
                    saver.setNewServer(address, port);

                } else if (key.equals(TraccarActivity.KEY_PORT)) {

                    port = Integer.valueOf(sharedPreferences.getString(TraccarActivity.KEY_PORT, null));
                    //clientController.setNewServer(address, port);
                    saver.setNewServer(address, port);

                } else if (key.equals(TraccarActivity.KEY_INTERVAL)) {

                    interval = Integer.valueOf(sharedPreferences.getString(TraccarActivity.KEY_INTERVAL, null));
                    positionProvider.stopUpdates();
                    positionProvider = new PositionProvider(TraccarService.this, provider, interval * 1000, positionListener, clock);
                    positionProvider.startUpdates();

                } else if (key.equals(TraccarActivity.KEY_ID)) {

                    id = sharedPreferences.getString(TraccarActivity.KEY_ID, null);

                }
            } catch (Exception error) {
                Log.w(LOG_TAG, error);
            }
        }

    };


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getApplicationContext(), this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() +2000, restartServicePI);
    }

    public void turnGPSOn()
    {
        Log.d(LOG_TAG,"Try turn GPS on");

        String provider = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            this.sendBroadcast(poke);

        }
    }
//    public Bundle getInfoFor1C() {
//        Bundle bundle = new Bundle();
//        bundle.putDouble("lat", saver.lastPoint.getLatitude());
//        bundle.putDouble("lng", saver.lastPoint.getLongitude());
//        bundle.putLong("time", saver.lastPoint.getTime());
//        bundle.putString("guid", id);
//        return bundle;
//    }

}
